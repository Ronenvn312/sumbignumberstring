package com.dat.test.TestITD.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dat.test.TestITD.model.MyBigNumber;

@RestController
@RequestMapping("/")
public class NumberController {
	
	@GetMapping("/sum")
	public String sum(@RequestParam String stn1, @RequestParam String stn2) {
		MyBigNumber mbn = new MyBigNumber();
		return mbn.sum(stn1, stn2);
	}

}
