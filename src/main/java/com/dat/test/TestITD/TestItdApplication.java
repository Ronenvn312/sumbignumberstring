package com.dat.test.TestITD;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.dat.test.TestITD.model.MyBigNumber;

@SpringBootApplication
public class TestItdApplication {
	static Logger logger = Logger.getLogger(TestItdApplication.class.getName());
	public static void main(String[] args) {
		SpringApplication.run(TestItdApplication.class, args);
	}

}
