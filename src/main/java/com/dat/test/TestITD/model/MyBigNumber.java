package com.dat.test.TestITD.model;

/* 
 * Auth: Nguyen Tien Dat
 * **/
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyBigNumber {
	Logger logger = Logger.getLogger(MyBigNumber.class.getName());

	public String sum(String stn1, String stn2) {
		// khai bao bien ket qua xay dung chuoi
		StringBuilder ketQua = new StringBuilder();
		// khai bao 2 bien do dai cua 2 chuoi
		int n1 = stn1.length();
		int n2 = stn2.length();
		// biến chứa phần nhớ
		int soNho = 0;
		// Tạo vòng lặp
		for (int i = 0; i < Math.max(n1, n2); i++) {
			// Khai báo biến chứa ký tự từ cuối chuỗi tới đầu của từng chuỗi,
			// nếu số lần lặp bé hơn độ dài chuỗi thì biến bằng ký tự thư n- 1- i,
			//còn ngược lại mặc định biến = 0
			int digit1 = i < n1 ? stn1.charAt(n1 - 1 - i) - '0' : 0;
			int digit2 = i < n2 ? stn2.charAt(n2 - 1 - i) - '0' : 0;
			//Tính tổng 
			int sum = digit1 + digit2 + soNho;
			// chia lấy phần dư
			int digit = sum % 10;
			// chia lấy phần nguyên
			soNho = sum / 10;
			//gán số vào đầu chuỗi kết quả
			ketQua.append(digit);
			logger.log(Level.WARNING,
					digit1 + " + " + digit2 + " + " + soNho + " = " + sum + ", write " + digit + ", remember " + soNho);
		}
		//nếu hết vòng lặp vẫn còn nhớ thì gán số nhớ vào đầu chuỗi kết quả
		if (soNho > 0) {
			ketQua.append(soNho);
			logger.log(Level.WARNING, "Write " + soNho + " (last soNho)");
		}
		logger.log(Level.WARNING, ketQua.reverse().toString());
		return ketQua.reverse().toString();
	}
}